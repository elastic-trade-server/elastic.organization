# -*- coding: utf-8 -*-

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from models import *
from forms import OuForm, OrderForm, OrderRowForm, DeliveryTermForm
from mpttadmin.admin import MpttAdmin
import signals

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Organization package"


def start_linked_workflow(modeladmin, request, queryset):
    for document in queryset.iterator():
        signals.workflow_restart.send(instance=document, sender=modeladmin)
start_linked_workflow.short_description = _('Start linked workflow')


def delete_linked_workflow(modeladmin, request, queryset):
    for document in queryset.iterator():
        signals.workflow_delete.send_robust(instance=document, sender=modeladmin)
delete_linked_workflow.short_description = _('Delete linked workflow')


def require_removal_replication(modeladmin, request, queryset):
    for item in queryset.iterator():
        signals.replication_request.send(sender=item, **{'is_removing': True})
require_removal_replication.short_description = _('Distributed request on removal')


def require_replication(modeladmin, request, queryset):
    for item in queryset.iterator():
        signals.replication_request.send(sender=item)
require_replication.short_description = _('Distributed request on update')


class BankAccountInline(admin.TabularInline):
    model = BankAccount


class OrganizationAdmin(admin.ModelAdmin):
    list_display = ('name', 'reg_no', )
    inlines = [
        BankAccountInline,
    ]

admin.site.register(Organization, OrganizationAdmin)


admin.site.register(Employee, admin.ModelAdmin)


class OUAdmin(MpttAdmin):
    class Meta:
        model = OU

    tree_title_field = 'name'
    tree_display = ('name', 'organization', )
    form = OuForm

    fieldsets = (
        (None, {
            'fields':  ['parent', 'name', 'code', 'organization', 'party', ]
        }),
    )

admin.site.register(OU, OUAdmin)


class CounterpartyAdditionalParamInline(admin.TabularInline):
    model = CounterpartyAdditionalParam


class CounterpartyAdmin(admin.ModelAdmin):
    list_display = ('party', 'organization', 'tax_no', 'counterparty_type', )

    inlines = [
        CounterpartyAdditionalParamInline,
    ]

admin.site.register(Counterparty, CounterpartyAdmin)


class PaymentInline(admin.StackedInline):
    model = Payment


class DeliveryTermInline(admin.StackedInline):
    form = DeliveryTermForm
    model = DeliveryTerm


class OrderRowInline(admin.StackedInline):
    form = OrderRowForm
    model = OrderRow


class OrderAdmin(admin.ModelAdmin):
    list_display = ('doc_no', 'creation_date', 'last_modify_date', 'expired_date', 'customer_name',
                    'reason', 'build_date', 'is_succeeded_build', 'location_name', 'ou_name', )
    list_filter = ('creation_date', 'customer_name', 'location_name', 'ou_name', )
    search_fields = ('doc_no', 'code', 'rows__product_sku', 'customer_login', 'customer_phone', )
    date_hierarchy = 'creation_date'

    form = OrderForm

    actions = [require_removal_replication, require_replication, start_linked_workflow, delete_linked_workflow, ]

    inlines = [
        PaymentInline,
        DeliveryTermInline,
        OrderRowInline,
    ]

admin.site.register(Order, OrderAdmin)


def request_next_number(modeladmin, request, queryset):
    for document in queryset.iterator():
        document.request_next_number()
request_next_number.short_description = _('Request next number')


class SequenceAdmin(admin.ModelAdmin):
    list_display = ('name', 'current_number', )
    readonly_fields = ('number', )

    actions = [request_next_number, ]

admin.site.register(Sequence, SequenceAdmin)

