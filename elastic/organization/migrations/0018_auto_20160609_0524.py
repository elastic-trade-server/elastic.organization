# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0016_auto_20160513_0855'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='total_accrued_bp',
            field=models.IntegerField(default=0, null=True, verbose_name='Total accrued bonus points', blank=True),
        ),
        migrations.AlterField(
            model_name='orderrow',
            name='accrued_bp',
            field=models.IntegerField(default=0, null=True, verbose_name='Accrued bonus points', blank=True),
        ),
    ]
