# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('delivery', '0001_initial'),
        ('address', '__first__'),
        ('party', '__first__'),
        ('organization', '0002_auto_20160120_1730'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeliveryTerm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('incoterms_code', models.CharField(default=b'EXW', max_length=3, verbose_name='INCOTERMS code', choices=[(b'EXW', 'Ex worker'), (b'FCA', 'Free carrier'), (b'CPT', 'Carriage paid to'), (b'CIP', 'Carriage and insurance paid to'), (b'DAT', 'Delivered at terminal'), (b'DAP', 'Delivered at point'), (b'DDP', 'Delivered duty paid'), (b'FOB', 'Free on board'), (b'FAS', 'Free alongside ship'), (b'CFR', 'Cost and freight'), (b'CIF', 'Cost, Insurance and Freight')])),
                ('alternative_delivery_terms_code', models.CharField(max_length=56, null=True, verbose_name='Alternative delivery terms code', blank=True)),
                ('delivery_instruction', models.TextField(null=True, verbose_name='Delivery instruction', blank=True)),
                ('delivery_location', models.ForeignKey(verbose_name='Delivery location', blank=True, to='address.Address', null=True)),
            ],
            options={
                'verbose_name': 'Delivery term',
                'verbose_name_plural': 'Delivery terms',
            },
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(verbose_name='Creation date')),
                ('payment', models.DecimalField(verbose_name='Payment', max_digits=15, decimal_places=2)),
                ('payment_type', models.CharField(max_length=8, verbose_name='Payment type', choices=[(b'bp', 'Bonus point'), (b'cert', 'Certified'), (b'eft', 'Electronic funds transfer'), (b'cash', 'Cash')])),
                ('transaction_no', models.CharField(max_length=128, null=True, verbose_name='Transaction No', blank=True)),
            ],
            options={
                'verbose_name': 'Payment',
                'verbose_name_plural': 'Payments',
            },
        ),
        migrations.AddField(
            model_name='employee',
            name='code',
            field=models.CharField(max_length=16, null=True, verbose_name='Code', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='auth_code',
            field=models.CharField(max_length=64, null=True, verbose_name='Temporary code', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='carrier',
            field=models.ForeignKey(verbose_name='Carrier', blank=True, to='delivery.Carrier', null=True),
        ),
        migrations.AddField(
            model_name='order',
            name='consignee',
            field=models.ForeignKey(related_name='consignies', verbose_name='Consignee', blank=True, to='party.Party', null=True),
        ),
        migrations.AddField(
            model_name='order',
            name='dc_card_no',
            field=models.CharField(max_length=1024, null=True, verbose_name='Discount card No', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='ou_code',
            field=models.CharField(default='0001', max_length=1024, verbose_name='OU code'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='ou_name',
            field=models.CharField(default='Some OU', max_length=1024, verbose_name='OU name'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='promo_code',
            field=models.CharField(max_length=1024, null=True, verbose_name='Promo code', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='total_accrued_bp',
            field=models.IntegerField(null=True, verbose_name='Total accrued bonus points', blank=True),
        ),
        migrations.AddField(
            model_name='orderrow',
            name='accrued_bp',
            field=models.IntegerField(null=True, verbose_name='Accrued bonus points', blank=True),
        ),
        migrations.AddField(
            model_name='orderrow',
            name='discount',
            field=models.DecimalField(null=True, verbose_name='Discount', max_digits=15, decimal_places=2, blank=True),
        ),
        migrations.AddField(
            model_name='orderrow',
            name='discount_percentage',
            field=models.DecimalField(null=True, verbose_name='Discount percentage', max_digits=5, decimal_places=2, blank=True),
        ),
        migrations.AddField(
            model_name='orderrow',
            name='product_category_code',
            field=models.CharField(max_length=128, null=True, verbose_name='Product category code', blank=True),
        ),
        migrations.AddField(
            model_name='orderrow',
            name='product_category_name',
            field=models.CharField(max_length=255, null=True, verbose_name='Product category name', blank=True),
        ),
        migrations.AddField(
            model_name='orderrow',
            name='product_group_code',
            field=models.CharField(max_length=128, null=True, verbose_name='Product group code', blank=True),
        ),
        migrations.AddField(
            model_name='orderrow',
            name='product_group_name',
            field=models.CharField(max_length=255, null=True, verbose_name='Product group name', blank=True),
        ),
        migrations.AddField(
            model_name='orderrow',
            name='total_cost',
            field=models.DecimalField(default=b'0.00', verbose_name='Total cost', max_digits=15, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='order',
            name='expired_date',
            field=models.DateTimeField(null=True, verbose_name='Expired date', blank=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(max_length=10, verbose_name='Status', choices=[(b'NEW', 'New'), (b'UNPAID', 'Unpaid'), (b'PAID', 'Paid'), (b'ABORT', 'Abort'), (b'COMPLETED', 'Completed')]),
        ),
        migrations.AddField(
            model_name='payment',
            name='order',
            field=models.ForeignKey(related_name='payments', verbose_name='Order', to='organization.Order'),
        ),
        migrations.AddField(
            model_name='deliveryterm',
            name='order',
            field=models.OneToOneField(related_name='delivery_term', verbose_name='Order', to='organization.Order'),
        ),
    ]
