# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import mptt.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('party', '__first__'),
        ('currency', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='BankAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('account', models.CharField(max_length=255, verbose_name='Account')),
                ('c_account', models.CharField(max_length=255, verbose_name='Correspondent account')),
                ('bank_name', models.CharField(max_length=255, verbose_name='Name')),
                ('bank_code', models.CharField(max_length=128, verbose_name='Bank ID code')),
            ],
            options={
                'verbose_name': 'Bank account',
                'verbose_name_plural': 'Bank accounts',
            },
        ),
        migrations.CreateModel(
            name='Counterparty',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tax_no', models.CharField(max_length=16, null=True, verbose_name='Tax number', blank=True)),
            ],
            options={
                'verbose_name': 'Counterparty',
                'verbose_name_plural': 'Counterparties',
            },
        ),
        migrations.CreateModel(
            name='CounterpartyAdditionalParam',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name='Name')),
                ('value', models.CharField(max_length=255, verbose_name='Value')),
                ('counterparty', models.ForeignKey(related_name='additional_params', to='organization.Counterparty')),
            ],
            options={
                'verbose_name': 'Additional feature',
                'verbose_name_plural': 'Additional features',
            },
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rank', models.CharField(max_length=255, verbose_name='Rank')),
            ],
            options={
                'verbose_name': 'Employee',
                'verbose_name_plural': 'Employees',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('doc_no', models.CharField(max_length=128, verbose_name='Document number', db_index=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='Creation date')),
                ('expired_date', models.DateTimeField(verbose_name='Expired date')),
                ('delivery_cost', models.DecimalField(decimal_places=2, default=b'0.00', max_digits=15, blank=True, null=True, verbose_name='Delivery cost')),
                ('total_cost', models.DecimalField(decimal_places=2, default=b'0.00', max_digits=15, blank=True, null=True, verbose_name='Total cost')),
                ('grand_total', models.DecimalField(decimal_places=2, default=b'0.00', max_digits=15, blank=True, null=True, verbose_name='Grand total')),
                ('status', models.CharField(max_length=10, verbose_name='Status', choices=[(b'NEW', 'New'), (b'UNPAID', 'Unpaid'), (b'PAID', 'Paid'), (b'ABORT', 'Abort'), (b'PENDING', 'Pending'), (b'ACCEPTED', 'Accepted'), (b'COMPLETED', 'Completed')])),
                ('solution', models.CharField(blank=True, max_length=15, null=True, verbose_name='Solution', choices=[(b'SUCCESS', 'Success'), (b'TIMEOUT_ERROR', 'Timeout error'), (b'USER_CANCELED', 'User canceled'), (b'INTERNAL_ERROR', 'Internal error')])),
                ('description', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('currency', models.ForeignKey(verbose_name='Currency', to='currency.Currency')),
                ('customer', models.ForeignKey(verbose_name='Customer', to='organization.Counterparty')),
            ],
            options={
                'verbose_name': 'Order',
                'verbose_name_plural': 'Orders',
            },
        ),
        migrations.CreateModel(
            name='OrderRow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('location_code', models.CharField(max_length=1024, verbose_name='Location code')),
                ('location_name', models.CharField(max_length=1024, verbose_name='Location name')),
                ('product_sku', models.CharField(max_length=1024, verbose_name='Product SKU')),
                ('product_name', models.CharField(max_length=1024, verbose_name='Product name')),
                ('manufacturer_code', models.CharField(max_length=1024, null=True, verbose_name='Manufacturer code', blank=True)),
                ('consignment_country', models.CharField(max_length=16, null=True, verbose_name='Country', blank=True)),
                ('retail_price', models.DecimalField(null=True, verbose_name='Retail price', max_digits=15, decimal_places=2, blank=True)),
                ('uom', models.CharField(max_length=16, verbose_name='Unit of measures')),
                ('quantity', models.DecimalField(verbose_name='Quantity', max_digits=12, decimal_places=3)),
                ('order', models.ForeignKey(related_name='rows', verbose_name='Order', to='organization.Order')),
            ],
            options={
                'verbose_name': 'Order row',
                'verbose_name_plural': 'Order rows',
            },
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name', db_index=True)),
                ('reg_no', models.CharField(max_length=128, verbose_name='State registration number', db_index=True)),
                ('crr', models.CharField(max_length=16, null=True, verbose_name='Code of reason for registration', blank=True)),
            ],
            options={
                'verbose_name': 'Organization',
                'verbose_name_plural': 'Organizations',
            },
        ),
        migrations.CreateModel(
            name='OU',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('organization', models.ForeignKey(verbose_name='Organization', blank=True, to='organization.Organization', null=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', verbose_name='Parent', blank=True, to='organization.OU', null=True)),
                ('party', models.ForeignKey(verbose_name='Party', blank=True, to='party.Party', null=True)),
            ],
            options={
                'verbose_name': 'Organization unit',
                'verbose_name_plural': 'Organization units',
            },
        ),
        migrations.CreateModel(
            name='Sequence',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('separator', models.CharField(default=b'-', max_length=1, verbose_name='Separator', blank=True)),
                ('prefix', models.CharField(max_length=8, null=True, verbose_name='Prefix', blank=True)),
                ('number', models.IntegerField(default=0, verbose_name='Number', editable=False)),
                ('suffix', models.CharField(max_length=8, null=True, verbose_name='Suffix', blank=True)),
            ],
            options={
                'verbose_name': 'Sequence',
                'verbose_name_plural': 'Sequences',
            },
        ),
        migrations.AddField(
            model_name='employee',
            name='ou',
            field=models.ForeignKey(verbose_name='Org unit', blank=True, to='organization.OU', null=True),
        ),
        migrations.AddField(
            model_name='employee',
            name='party',
            field=models.ForeignKey(verbose_name='Party', blank=True, to='party.Party', null=True),
        ),
        migrations.AddField(
            model_name='employee',
            name='user',
            field=models.OneToOneField(verbose_name='User account', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='counterparty',
            name='organization',
            field=models.ForeignKey(verbose_name='Organization', blank=True, to='organization.Organization', null=True),
        ),
        migrations.AddField(
            model_name='counterparty',
            name='party',
            field=models.ForeignKey(verbose_name='Party', blank=True, to='party.Party', null=True),
        ),
        migrations.AddField(
            model_name='counterparty',
            name='user',
            field=models.ForeignKey(verbose_name='Site user', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='bankaccount',
            name='organization',
            field=models.ForeignKey(related_name='bank_accounts', verbose_name='Organization', to='organization.Organization'),
        ),
    ]
