# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0006_auto_20160418_0548'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='build_act_code',
        ),
        migrations.AddField(
            model_name='order',
            name='build_date',
            field=models.DateTimeField(null=True, verbose_name='Build date', blank=True),
        ),
    ]
