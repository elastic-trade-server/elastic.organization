# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0014_auto_20160428_0604'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='dc_engine_id',
            field=models.CharField(max_length=128, null=True, verbose_name='Discount system engine ID', blank=True),
        ),
        migrations.AlterField(
            model_name='counterparty',
            name='user',
            field=models.OneToOneField(verbose_name='Site user', to=settings.AUTH_USER_MODEL),
        ),
    ]
