# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0004_auto_20160418_0444'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='consignee',
        ),
        migrations.RemoveField(
            model_name='order',
            name='customer',
        ),
        migrations.AddField(
            model_name='order',
            name='consignee_name',
            field=models.CharField(max_length=255, null=True, verbose_name='Consignee name', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='consignee_phone',
            field=models.CharField(max_length=255, null=True, verbose_name='Consignee phone', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='customer_login',
            field=models.CharField(max_length=255, null=True, verbose_name='Customer login', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='customer_name',
            field=models.CharField(max_length=255, null=True, verbose_name='Customer name', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='customer_phone',
            field=models.CharField(max_length=255, null=True, verbose_name='Customer phone', blank=True),
        ),
    ]
