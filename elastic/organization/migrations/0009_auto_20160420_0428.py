# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0008_auto_20160420_0146'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderrow',
            name='difference',
            field=models.DecimalField(null=True, verbose_name='Difference', max_digits=12, decimal_places=3, blank=True),
        ),
        migrations.AlterField(
            model_name='orderrow',
            name='total_cost',
            field=models.DecimalField(verbose_name='Total cost', max_digits=15, decimal_places=2),
        ),
    ]
