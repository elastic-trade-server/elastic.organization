# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0003_auto_20160201_1126'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='build_act_code',
            field=models.CharField(max_length=128, null=True, verbose_name='Build act code', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='due_date',
            field=models.DateTimeField(null=True, verbose_name='Ended date', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='receipt_code',
            field=models.CharField(max_length=128, null=True, verbose_name='Receipt code', blank=True),
        ),
        migrations.AddField(
            model_name='orderrow',
            name='difference',
            field=models.DecimalField(default=0, verbose_name='Difference', max_digits=12, decimal_places=3),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ou',
            name='code',
            field=models.CharField(max_length=16, null=True, verbose_name='Code', blank=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(max_length=10, verbose_name='Status', choices=[(b'NEW', 'New'), (b'PROCESSING', 'Processing'), (b'WAIT', 'Wait'), (b'COLLECTED', 'Collected'), (b'ABORT', 'Abort'), (b'COMPLETED', 'Completed')]),
        ),
    ]
