# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0013_auto_20160427_0425'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='reason',
            field=models.CharField(max_length=1024, null=True, verbose_name='Reason', blank=True),
        ),
        migrations.AlterField(
            model_name='employee',
            name='code',
            field=models.CharField(max_length=16, verbose_name='Personnel number'),
        ),
        migrations.AlterField(
            model_name='ou',
            name='code',
            field=models.CharField(max_length=16, verbose_name='Code'),
        ),
    ]
