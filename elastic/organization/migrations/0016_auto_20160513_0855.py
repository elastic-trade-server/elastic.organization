# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0015_auto_20160504_0848'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='create_by',
            new_name='created_by',
        ),
    ]
