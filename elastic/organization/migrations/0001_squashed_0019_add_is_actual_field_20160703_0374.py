# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import mptt.fields
from django.utils.timezone import utc
import datetime
from django.conf import settings

def update_is_actual(apps, schema_editor):
    Order = apps.get_model('organization', 'order')
    for order in Order.objects.iterator():
        actual_order = Order.objects.filter(doc_no=order.doc_no).order_by('-creation_date').first()
        if actual_order:
            order.is_actual = actual_order.id == order.id
        else:
            order.is_actual = True
        order.save()


# Functions from the following migrations need manual copying.
# Move them and any dependencies into this file, then update the
# RunPython operations to refer to the local versions:
# elastic.organization.migrations.0019_add_is_actual_field_20160703_0374

class Migration(migrations.Migration):

    replaces = [(b'organization', '0001_initial'), (b'organization', '0002_auto_20160120_1730'), (b'organization', '0003_auto_20160201_1126'), (b'organization', '0004_auto_20160418_0444'), (b'organization', '0005_auto_20160418_0534'), (b'organization', '0006_auto_20160418_0548'), (b'organization', '0007_auto_20160419_0714'), (b'organization', '0008_auto_20160420_0146'), (b'organization', '0009_auto_20160420_0428'), (b'organization', '0010_auto_20160421_0711'), (b'organization', '0011_auto_20160425_0308'), (b'organization', '0012_auto_20160425_0338'), (b'organization', '0013_auto_20160427_0425'), (b'organization', '0014_auto_20160428_0604'), (b'organization', '0015_auto_20160504_0848'), (b'organization', '0016_auto_20160513_0855'), (b'organization', '0018_auto_20160609_0524'), (b'organization', '0019_add_is_actual_field_20160703_0374')]

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('currency', '__first__'),
        ('product', '0001_initial'),
        ('party', '__first__'),
        ('delivery', '0001_initial'),
        ('address', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='BankAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('account', models.CharField(max_length=255, verbose_name='Account')),
                ('c_account', models.CharField(max_length=255, verbose_name='Correspondent account')),
                ('bank_name', models.CharField(max_length=255, verbose_name='Name')),
                ('bank_code', models.CharField(max_length=128, verbose_name='Bank ID code')),
            ],
            options={
                'verbose_name': 'Bank account',
                'verbose_name_plural': 'Bank accounts',
            },
        ),
        migrations.CreateModel(
            name='Counterparty',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tax_no', models.CharField(max_length=16, null=True, verbose_name='Tax number', blank=True)),
            ],
            options={
                'verbose_name': 'Counterparty',
                'verbose_name_plural': 'Counterparties',
            },
        ),
        migrations.CreateModel(
            name='CounterpartyAdditionalParam',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name='Name')),
                ('value', models.CharField(max_length=255, verbose_name='Value')),
                ('counterparty', models.ForeignKey(related_name='additional_params', to='organization.Counterparty')),
            ],
            options={
                'verbose_name': 'Additional feature',
                'verbose_name_plural': 'Additional features',
            },
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rank', models.CharField(max_length=255, verbose_name='Rank')),
            ],
            options={
                'verbose_name': 'Employee',
                'verbose_name_plural': 'Employees',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('doc_no', models.CharField(max_length=128, verbose_name='Document number', db_index=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='Creation date')),
                ('expired_date', models.DateTimeField(verbose_name='Expired date')),
                ('delivery_cost', models.DecimalField(decimal_places=2, default=b'0.00', max_digits=15, blank=True, null=True, verbose_name='Delivery cost')),
                ('total_cost', models.DecimalField(decimal_places=2, default=b'0.00', max_digits=15, blank=True, null=True, verbose_name='Total cost')),
                ('grand_total', models.DecimalField(decimal_places=2, default=b'0.00', max_digits=15, blank=True, null=True, verbose_name='Grand total')),
                ('status', models.CharField(max_length=10, verbose_name='Status', choices=[(b'NEW', 'New'), (b'UNPAID', 'Unpaid'), (b'PAID', 'Paid'), (b'ABORT', 'Abort'), (b'PENDING', 'Pending'), (b'ACCEPTED', 'Accepted'), (b'COMPLETED', 'Completed')])),
                ('solution', models.CharField(blank=True, max_length=15, null=True, verbose_name='Solution', choices=[(b'SUCCESS', 'Success'), (b'TIMEOUT_ERROR', 'Timeout error'), (b'USER_CANCELED', 'User canceled'), (b'INTERNAL_ERROR', 'Internal error')])),
                ('description', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('currency', models.ForeignKey(verbose_name='Currency', to='currency.Currency')),
            ],
            options={
                'verbose_name': 'Order',
                'verbose_name_plural': 'Orders',
            },
        ),
        migrations.CreateModel(
            name='OrderRow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product_sku', models.CharField(max_length=1024, verbose_name='Product SKU')),
                ('product_name', models.CharField(max_length=1024, verbose_name='Product name')),
                ('manufacturer_code', models.CharField(max_length=1024, null=True, verbose_name='Manufacturer code', blank=True)),
                ('retail_price', models.DecimalField(verbose_name='Retail price', max_digits=15, decimal_places=2)),
                ('quantity', models.DecimalField(verbose_name='Quantity', max_digits=12, decimal_places=3)),
                ('order', models.ForeignKey(related_name='rows', verbose_name='Order', to='organization.Order')),
                ('accrued_bp', models.IntegerField(default=0, null=True, verbose_name='Accrued bonus points', blank=True)),
                ('discount', models.DecimalField(null=True, verbose_name='Discount', max_digits=15, decimal_places=2, blank=True)),
                ('discount_percentage', models.DecimalField(null=True, verbose_name='Discount percentage', max_digits=5, decimal_places=2, blank=True)),
                ('product_category_code', models.CharField(max_length=128, null=True, verbose_name='Product category code', blank=True)),
                ('product_category_name', models.CharField(max_length=255, null=True, verbose_name='Product category name', blank=True)),
                ('product_group_code', models.CharField(max_length=128, null=True, verbose_name='Product group code', blank=True)),
                ('product_group_name', models.CharField(max_length=255, null=True, verbose_name='Product group name', blank=True)),
                ('total_cost', models.DecimalField(verbose_name='Total cost', max_digits=15, decimal_places=2)),
                ('difference', models.DecimalField(null=True, verbose_name='Difference', max_digits=12, decimal_places=3, blank=True)),
                ('consignment_country', models.ForeignKey(verbose_name='Country of consignment', blank=True, to='country.Country', null=True)),
                ('uom', models.ForeignKey(verbose_name='Unit of measures', to='product.Uom')),
            ],
            options={
                'verbose_name': 'Order row',
                'verbose_name_plural': 'Order rows',
            },
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name', db_index=True)),
                ('reg_no', models.CharField(max_length=128, verbose_name='State registration number', db_index=True)),
                ('crr', models.CharField(max_length=16, null=True, verbose_name='Code of reason for registration', blank=True)),
            ],
            options={
                'verbose_name': 'Organization',
                'verbose_name_plural': 'Organizations',
            },
        ),
        migrations.CreateModel(
            name='OU',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('organization', models.ForeignKey(verbose_name='Organization', blank=True, to='organization.Organization', null=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', verbose_name='Parent', blank=True, to='organization.OU', null=True)),
                ('party', models.ForeignKey(verbose_name='Party', blank=True, to='party.Party', null=True)),
                ('code', models.CharField(max_length=16, verbose_name='Code')),
            ],
            options={
                'verbose_name': 'Organization unit',
                'verbose_name_plural': 'Organization units',
            },
        ),
        migrations.CreateModel(
            name='Sequence',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('separator', models.CharField(default=b'-', max_length=1, verbose_name='Separator', blank=True)),
                ('prefix', models.CharField(max_length=8, null=True, verbose_name='Prefix', blank=True)),
                ('number', models.IntegerField(default=0, verbose_name='Number', editable=False)),
                ('suffix', models.CharField(max_length=8, null=True, verbose_name='Suffix', blank=True)),
            ],
            options={
                'verbose_name': 'Sequence',
                'verbose_name_plural': 'Sequences',
            },
        ),
        migrations.AddField(
            model_name='employee',
            name='ou',
            field=models.ForeignKey(verbose_name='Org unit', blank=True, to='organization.OU', null=True),
        ),
        migrations.AddField(
            model_name='employee',
            name='party',
            field=models.ForeignKey(verbose_name='Party', blank=True, to='party.Party', null=True),
        ),
        migrations.AddField(
            model_name='employee',
            name='user',
            field=models.OneToOneField(verbose_name='User account', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='counterparty',
            name='organization',
            field=models.ForeignKey(verbose_name='Organization', blank=True, to='organization.Organization', null=True),
        ),
        migrations.AddField(
            model_name='counterparty',
            name='party',
            field=models.ForeignKey(verbose_name='Party', blank=True, to='party.Party', null=True),
        ),
        migrations.AddField(
            model_name='counterparty',
            name='user',
            field=models.OneToOneField(verbose_name='Site user', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='bankaccount',
            name='organization',
            field=models.ForeignKey(related_name='bank_accounts', verbose_name='Organization', to='organization.Organization'),
        ),
        migrations.AddField(
            model_name='counterparty',
            name='last_modify_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 20, 17, 30, 11, 67398, tzinfo=utc), verbose_name='Last modify date'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='code',
            field=models.CharField(max_length=128, null=True, verbose_name='Code', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='effective_date',
            field=models.DateTimeField(null=True, verbose_name='Effective date', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='last_modify_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 20, 17, 30, 20, 338831, tzinfo=utc), verbose_name='Last modify date'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='location_code',
            field=models.CharField(default=datetime.datetime(2016, 1, 20, 17, 30, 22, 792682, tzinfo=utc), max_length=1024, verbose_name='Location code'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='location_name',
            field=models.CharField(default=datetime.datetime(2016, 1, 20, 17, 30, 27, 388083, tzinfo=utc), max_length=1024, verbose_name='Location name'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='order',
            name='creation_date',
            field=models.DateTimeField(verbose_name='Creation date'),
        ),
        migrations.CreateModel(
            name='DeliveryTerm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('incoterms_code', models.CharField(default=b'EXW', max_length=3, verbose_name='INCOTERMS code', choices=[(b'EXW', 'Ex worker'), (b'FCA', 'Free carrier'), (b'CPT', 'Carriage paid to'), (b'CIP', 'Carriage and insurance paid to'), (b'DAT', 'Delivered at terminal'), (b'DAP', 'Delivered at point'), (b'DDP', 'Delivered duty paid'), (b'FOB', 'Free on board'), (b'FAS', 'Free alongside ship'), (b'CFR', 'Cost and freight'), (b'CIF', 'Cost, Insurance and Freight')])),
                ('alternative_delivery_terms_code', models.CharField(max_length=56, null=True, verbose_name='Alternative delivery terms code', blank=True)),
                ('delivery_instruction', models.TextField(null=True, verbose_name='Delivery instruction', blank=True)),
                ('delivery_location', models.ForeignKey(verbose_name='Delivery location', blank=True, to='address.Address', null=True)),
            ],
            options={
                'verbose_name': 'Delivery term',
                'verbose_name_plural': 'Delivery terms',
            },
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(verbose_name='Creation date')),
                ('payment', models.DecimalField(verbose_name='Payment', max_digits=15, decimal_places=2)),
                ('payment_type', models.CharField(max_length=8, verbose_name='Payment type', choices=[(b'bp', 'Bonus point'), (b'cert', 'Certified'), (b'eft', 'Electronic funds transfer'), (b'cash', 'Cash')])),
                ('transaction_no', models.CharField(max_length=128, null=True, verbose_name='Transaction No', blank=True)),
            ],
            options={
                'verbose_name': 'Payment',
                'verbose_name_plural': 'Payments',
            },
        ),
        migrations.AddField(
            model_name='employee',
            name='code',
            field=models.CharField(max_length=16, verbose_name='Personnel number'),
        ),
        migrations.AddField(
            model_name='order',
            name='auth_code',
            field=models.CharField(max_length=64, null=True, verbose_name='Authorization code', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='dc_card_no',
            field=models.CharField(max_length=1024, null=True, verbose_name='Discount card No', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='ou_code',
            field=models.CharField(default='0001', max_length=1024, verbose_name='OU code'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='ou_name',
            field=models.CharField(default='Some OU', max_length=1024, verbose_name='OU name'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='promo_code',
            field=models.CharField(max_length=1024, null=True, verbose_name='Promo code', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='total_accrued_bp',
            field=models.IntegerField(default=0, null=True, verbose_name='Total accrued bonus points', blank=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='expired_date',
            field=models.DateTimeField(null=True, verbose_name='Expired date', blank=True),
        ),
        migrations.RemoveField(
            model_name='order',
            name='status',
        ),
        migrations.AddField(
            model_name='payment',
            name='order',
            field=models.ForeignKey(related_name='payments', verbose_name='Order', to='organization.Order'),
        ),
        migrations.AddField(
            model_name='deliveryterm',
            name='order',
            field=models.OneToOneField(related_name='delivery_term', verbose_name='Order', to='organization.Order'),
        ),
        migrations.AddField(
            model_name='order',
            name='due_date',
            field=models.DateTimeField(null=True, verbose_name='Ended date', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='consignee_name',
            field=models.CharField(max_length=255, null=True, verbose_name='Consignee name', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='consignee_phone',
            field=models.CharField(max_length=255, null=True, verbose_name='Consignee phone', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='customer_login',
            field=models.CharField(max_length=255, null=True, verbose_name='Customer login', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='customer_name',
            field=models.CharField(max_length=255, verbose_name='Customer name'),
        ),
        migrations.AddField(
            model_name='order',
            name='customer_phone',
            field=models.CharField(max_length=255, null=True, verbose_name='Customer phone', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='build_date',
            field=models.DateTimeField(null=True, verbose_name='Build date', blank=True),
        ),
        migrations.RemoveField(
            model_name='order',
            name='solution',
        ),
        migrations.AddField(
            model_name='order',
            name='carrier_code',
            field=models.CharField(max_length=64, null=True, verbose_name='Carrier code', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='carrier_name',
            field=models.CharField(max_length=255, null=True, verbose_name='Carrier name', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='created_by',
            field=models.CharField(default='dev1', max_length=64, verbose_name='Node creator'),
        ),
        migrations.AddField(
            model_name='order',
            name='reason',
            field=models.CharField(max_length=1024, null=True, verbose_name='Reason', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='dc_engine_id',
            field=models.CharField(max_length=128, null=True, verbose_name='Discount system engine ID', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='is_actual',
            field=models.BooleanField(default=True),
        ),
        migrations.RunPython(
            code=update_is_actual,
        ),
    ]
