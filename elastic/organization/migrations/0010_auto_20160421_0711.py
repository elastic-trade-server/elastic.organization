# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0009_auto_20160420_0428'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='receipt_code',
        ),
        migrations.AlterField(
            model_name='orderrow',
            name='retail_price',
            field=models.DecimalField(verbose_name='Retail price', max_digits=15, decimal_places=2),
        ),
    ]
