# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0007_auto_20160419_0714'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='solution',
        ),
        migrations.RemoveField(
            model_name='order',
            name='status',
        ),
        migrations.AlterField(
            model_name='employee',
            name='code',
            field=models.CharField(max_length=16, null=True, verbose_name='Personnel number', blank=True),
        ),
    ]
