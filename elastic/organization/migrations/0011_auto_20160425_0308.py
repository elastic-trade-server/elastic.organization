# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models





class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0010_auto_20160421_0711'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='auth_code',
            field=models.CharField(max_length=64, null=True, verbose_name='Authorization code', blank=True),
        ),
        migrations.RemoveField(
            model_name='orderrow',
            name='consignment_country'
        ),
        migrations.AddField(
            model_name='orderrow',
            name='consignment_country',
            field=models.ForeignKey(verbose_name='Country of consignment', blank=True, to='country.Country', null=True),
        ),
    ]
