# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def update_is_actual(apps, schema_editor):
    Order = apps.get_model('organization', 'order')
    for order in Order.objects.iterator():
        actual_order = Order.objects.filter(doc_no=order.doc_no).order_by('-creation_date').first()
        if actual_order:
            order.is_actual = actual_order.id == order.id
        else:
            order.is_actual = True
        order.save()


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0018_auto_20160609_0524'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='is_actual',
            field=models.BooleanField(default=True),
        ),
        migrations.RunPython(update_is_actual)
    ]
