# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='orderrow',
            name='location_code',
        ),
        migrations.RemoveField(
            model_name='orderrow',
            name='location_name',
        ),
        migrations.AddField(
            model_name='counterparty',
            name='last_modify_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 20, 17, 30, 11, 67398, tzinfo=utc), verbose_name='Last modify date'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='code',
            field=models.CharField(max_length=128, null=True, verbose_name='Code', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='effective_date',
            field=models.DateTimeField(null=True, verbose_name='Effective date', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='last_modify_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 20, 17, 30, 20, 338831, tzinfo=utc), verbose_name='Last modify date'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='location_code',
            field=models.CharField(default=datetime.datetime(2016, 1, 20, 17, 30, 22, 792682, tzinfo=utc), max_length=1024, verbose_name='Location code'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='location_name',
            field=models.CharField(default=datetime.datetime(2016, 1, 20, 17, 30, 27, 388083, tzinfo=utc), max_length=1024, verbose_name='Location name'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='order',
            name='creation_date',
            field=models.DateTimeField(verbose_name='Creation date'),
        ),
    ]
