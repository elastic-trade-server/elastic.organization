# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0012_auto_20160425_0338'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='carrier',
        ),
        migrations.AddField(
            model_name='order',
            name='carrier_code',
            field=models.CharField(max_length=64, null=True, verbose_name='Carrier code', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='carrier_name',
            field=models.CharField(max_length=255, null=True, verbose_name='Carrier name', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='create_by',
            field=models.CharField(default='dev1', max_length=64, verbose_name='Node creator'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='employee',
            name='code',
            field=models.CharField(max_length=16, default='1', verbose_name='Personnel number'),
        ),
        migrations.AlterField(
            model_name='ou',
            name='code',
            field=models.CharField(max_length=16, default='1', verbose_name='Code'),
        ),
    ]
