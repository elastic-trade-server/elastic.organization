# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0011_auto_20160425_0308'),
        ('product', '0001_initial')
    ]

    operations = [
        migrations.RemoveField(
            model_name='orderrow',
            name='uom'
        ),
        migrations.AddField(
            model_name='orderrow',
            name='uom',
            field=models.ForeignKey(verbose_name='Unit of measures', to='product.Uom'),
        ),
    ]
