# -*- coding: utf-8 -*-


from decimal import Decimal

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Organization package"


class DcEngine(object):
    """
    Абстрактный клас "Движок дисконта"
    """

    id = None  # type: string идентификатор движки дисконта

    @property
    def dc_card(self):
        """
        Вернуть экземпляр класса "Дисконтная карта"
        :return: string
        """
        raise NotImplemented

    @dc_card.setter
    def dc_card(self, value):
        """
        Указать движку идентификационную карту участника
        """
        raise NotImplemented

    @property
    def account(self):
        """
        Вернуть экземпляр класса "Накомпительный счет"
        :return: string
        """
        raise NotImplemented

    @property
    def promo_code(self):
        """
        Вернуть экземпляр класса "Промо-код"
        :return: string
        """
        raise NotImplemented

    @promo_code.setter
    def promo_code(self, value):
        """
        Указать движку номер промо-кода
        """
        raise NotImplemented

    def calculate_discount(self, product_sku, product_name, retail_price, quantity,
                           product_group_code=None, product_group_name=None,
                           product_category_code=None, product_category_name=None,
                           manufacturer_code=None,
                           last_result=Decimal("0.00")  # Результат предыдущего вычисления
                           ):
        """
        :param product_sku: Складской код (SKU) товара
        :param product_name: Наименование товара
        :param retail_price: Розничная цена
        :param quantity: Количество
        :param product_group_code: Код группы товара
        :param product_group_name: Наименование группы товара
        :param product_category_code: Код категории товара
        :param product_category_name: Наименование категории товара
        :param manufacturer_code: Код производителя (артикул)
        :param last_result: Результат предыдущего вычисления
        :return: Значение скидки в процентах
        """
        raise NotImplemented

    def calculate_bp(self, product_sku, product_name, retail_price, quantity,
                     product_group_code=None, product_group_name=None,
                     product_category_code=None, product_category_name=None,
                     manufacturer_code=None,
                     last_result=0
                     ):
        """
        :param product_sku: Складской код (SKU) товара
        :param product_name: Наименование товара
        :param retail_price: Розничная цена
        :param quantity: Количество
        :param product_group_code: Код группы товара
        :param product_group_name: Наименование группы товара
        :param product_category_code: Код категории товара
        :param product_category_name: Наименование категории товара
        :param manufacturer_code: Код производителя (артикул)
        :param last_result: Результат предыдущего вычисления
        :return: Количество баллов к зачислению
        """
        raise NotImplemented
