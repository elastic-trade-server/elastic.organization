# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth.models import User
from django.db import transaction
from elastic.country.models import Country
from elastic.currency.models import Currency
from elastic.address.models import Address
from elastic.party.models import Party
from elastic.product.models import Product, Uom
from elastic.stock.models import StockLocation, ReserveRegister, ReserveRegisterRow
import mptt
from mptt.models import MPTTModel, TreeForeignKey
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from datetime import timedelta
import uuid
from elastic.delivery.models import Carrier
from decimal import Decimal
from registry import registry
from elastic.workflow.models import Workflow, Schema
from django.conf import settings
from django.db.models.signals import pre_delete, post_save
from django.dispatch import receiver
from exception import *
from elastic.product.exceptions import ProductNotExists
from signals import workflow_restart, workflow_delete


__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Organization package"


@receiver(workflow_restart, dispatch_uid='workflow_restart')
def workflow_restart_handler(sender, instance, **kwargs):
    # Удаляем и заново создаем рабочий процесс (workflow) связанный с документом
    instance.get_workflow().delete()
    instance.get_workflow().start()


@receiver(workflow_delete, dispatch_uid='workflow_delete')
def workflow_delete_handler(sender, instance, **kwargs):
    # Удаляем рабочий процесс (workflow) связанный с документом
    instance.get_workflow().delete()


@receiver(pre_delete, dispatch_uid='org_pre_delete')
def pre_delete_handler(sender, instance, **kwargs):
    if sender in [Order, ]:
        # Удаляем рабочий процесс (workflow) связанный с документом
        workflow_delete.send(instance=instance, sender=sender)


class Organization(models.Model):
    """
    Организация
    """
    class Meta:
        verbose_name = _('Organization')
        verbose_name_plural = _('Organizations')

    #: Официальное наименование
    name = models.CharField(max_length=255, verbose_name=_('Name'), db_index=True)

    #: ОГРН/ОГРНИП
    reg_no = models.CharField(verbose_name=_('State registration number'), max_length=128, db_index=True)

    #: КПП
    crr = models.CharField(max_length=16, null=True, blank=True, verbose_name=_('Code of reason for registration'))

    def __unicode__(self):
        return self.name


class BankAccount(models.Model):
    """
    Расчетный счет
    """
    class Meta:
        verbose_name = _('Bank account')
        verbose_name_plural = _('Bank accounts')

    account = models.CharField(max_length=255, verbose_name=_('Account'))
    c_account = models.CharField(max_length=255, verbose_name=_('Correspondent account'))
    bank_name = models.CharField(verbose_name=_('Bank name'), max_length=255)
    bank_code = models.CharField(max_length=128, verbose_name=_('Bank ID code'))  # БИК
    organization = models.ForeignKey(Organization, related_name='bank_accounts', verbose_name=_('Organization'))

    def __unicode__(self):
        return unicode(self.account)


class OU(MPTTModel):
    """
    Организационное подразделение
    """
    class Meta:
        verbose_name = _('Organization unit')
        verbose_name_plural = _('Organization units')

    class MPTTMeta:
        parent_attr = 'parent'

    parent = TreeForeignKey('self', verbose_name=_('Parent'), null=True, blank=True, related_name='children')
    name = models.CharField(verbose_name=_('Name'), max_length=255)
    code = models.CharField(max_length=16, verbose_name=_('Code'))
    party = models.ForeignKey(Party, null=True, blank=True, verbose_name=_('Party'))
    organization = models.ForeignKey(Organization, null=True, blank=True, verbose_name=_('Organization'))

    def __unicode__(self):
        return self.name

mptt.register(OU)


class Employee(models.Model):
    """
    Сотрудник
    """
    class Meta:
        verbose_name = _('Employee')
        verbose_name_plural = _('Employees')

    code = models.CharField(max_length=16, verbose_name=_('Personnel number'))
    party = models.ForeignKey(Party, null=True, blank=True, verbose_name=_('Party'))
    user = models.OneToOneField(User, verbose_name=_('User account'))
    rank = models.CharField(verbose_name=_('Rank'), max_length=255)
    ou = models.ForeignKey(OU, null=True, blank=True, verbose_name=_('Org unit'))

    def __unicode__(self):
        return u"{0}".format(self.party)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.code:
            self.code = uuid.uuid1()
        super(Employee, self).save(force_insert, force_update, using, update_fields)


class Counterparty(models.Model):
    """
    Контрагент
    """
    class Meta:
        verbose_name = _('Counterparty')
        verbose_name_plural = _('Counterparties')

    #: Пользователь сайта
    user = models.OneToOneField(User, verbose_name=_('Site user'))

    #: Особа
    party = models.ForeignKey(Party, null=True, blank=True, verbose_name=_('Party'))

    #: Ссылка на элемент справочника "Организация"
    organization = models.ForeignKey(Organization, null=True, blank=True, verbose_name=_('Organization'))

    #: ИНН
    tax_no = models.CharField(max_length=16, null=True, blank=True, verbose_name=_('Tax number'))

    #: Дата/время последней модификации
    last_modify_date = models.DateTimeField(verbose_name=_('Last modify date'))

    def counterparty_type(self):
        return _("customer") if self.user else _("counterparty")
    counterparty_type.short_description = _('Counterparty type')

    def __unicode__(self):
        return u"{0}".format(self.party)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.last_modify_date:
            self.last_modify_date = timezone.now()

        super(Counterparty, self).save(force_insert, force_update, using, update_fields)


class CounterpartyAdditionalParam(models.Model):
    """
    Дополнительные данные по контрагенту
    """
    class Meta:
        verbose_name = _('Additional feature')
        verbose_name_plural = _('Additional features')

    name = models.CharField(verbose_name=_('Name'), max_length=128)
    value = models.CharField(verbose_name=_('Value'), max_length=255)
    counterparty = models.ForeignKey(Counterparty, related_name='additional_params')

    def __unicode__(self):
        return u"{0}: {1}".format(self.name, self.value)


class OrderManager(models.Manager):
    @staticmethod
    def get_actual_instance(doc_no):
        """
        Вернуть актуальный экземпляр заказа из пачки с заявленным номером
        """

        try:
            # Возвращаем сам документ, если он единственный в пачке
            return Order.objects.get(doc_no=doc_no)
        except:
            instance = None

        # Перебираем пачку в поисках документа на который никто не ссылается
        for order in Order.objects.filter(doc_no=doc_no):
            if Order.objects.filter(doc_no=doc_no, reason=order.code).count() == 0:
                instance = order
                break

        return instance

    @staticmethod
    def get_orders_by_workflow_satate(state):
        # Отбираем экземпляры рабочих потоков относящихся к заказам
        workflow_instances = Workflow.objects.filter(workflow_schema=Schema.objects.get(code='goods_order'))

        # Строим список ID документов заказа находящихся на нужном уровне
        order_pk_list = [w.doc_id if w.get_current_state_code() == state else None for w in workflow_instances]

        # Строим и возвращаем queryset отобранных заказов
        return Order.objects.filter(pk__in=order_pk_list)

    @staticmethod
    @transaction.atomic()
    def create_by_order(parent_order):
        """
        Создать корректирующий заказ, перенося в новый заказ номер, платежи и условия доставки
        """
        return parent_order.clone()


class Order(models.Model):
    """
    Заказ
    """
    class Meta:
        verbose_name = _('Order')
        verbose_name_plural = _('Orders')

    #: Номер документа
    doc_no = models.CharField(verbose_name=_('Document number'), max_length=128, db_index=True)  # Номер документа

    #: Внутренний код документа
    code = models.CharField(max_length=128, null=True, blank=True, verbose_name=_('Code'))

    #: Дата/время создания
    creation_date = models.DateTimeField(verbose_name=_('Creation date'))

    #: Дата/время последней модификации
    last_modify_date = models.DateTimeField(verbose_name=_('Last modify date'))

    #: Наименование узла-создателя документа
    created_by = models.CharField(max_length=64, verbose_name=_('Node creator'))

    #: Дата/время просрочки
    expired_date = models.DateTimeField(null=True, blank=True, verbose_name=_('Expired date'))

    #: Дата исполнения (резервирования)
    effective_date = models.DateTimeField(null=True, blank=True, verbose_name=_('Effective date'))

    #: Дата завершения заказа
    due_date = models.DateTimeField(null=True, blank=True, verbose_name=_('Due date'))

    #: Код склада розничной торговли
    location_code = models.CharField(max_length=1024, verbose_name=_('Location code'))

    #: Наименование склада розничной торговли
    location_name = models.CharField(max_length=1024, verbose_name=_('Location name'))

    #: Код подразделения выписавшего документ
    ou_code = models.CharField(max_length=1024, verbose_name=_('OU code'))

    #: Наименование подразделения выписавшего документ
    ou_name = models.CharField(max_length=1024, verbose_name=_('OU name'))

    #: ФИО или наименование покупателя
    customer_name = models.CharField(verbose_name=_('Customer name'), max_length=255)

    #: Учетное имя покупателя
    customer_login = models.CharField(null=True, blank=True, verbose_name=_('Customer login'), max_length=255)

    #: Контактный телефон покупателя
    customer_phone = models.CharField(null=True, blank=True, verbose_name=_('Customer phone'), max_length=255)

    #: ФИО или наименование грузополучателя
    consignee_name = models.CharField(null=True, blank=True, verbose_name=_('Consignee name'), max_length=255)

    #: Контактный телефон грузополучателя
    consignee_phone = models.CharField(null=True, blank=True, verbose_name=_('Consignee phone'), max_length=255)

    #: Код валюты
    currency = models.ForeignKey(Currency, verbose_name=_('Currency'))

    #: Итоговая стоимость
    total_cost = models.DecimalField(max_digits=15, decimal_places=2, default='0.00', null=True, blank=True,
                                     verbose_name=_('Total cost'))

    #: К оплате
    grand_total = models.DecimalField(max_digits=15, decimal_places=2, default='0.00', null=True, blank=True,
                                      verbose_name=_('Grand total'))

    #: Дата завершения сборки
    build_date = models.DateTimeField(null=True, blank=True, verbose_name=_('Build date'))

    #: Примечание
    description = models.TextField(null=True, blank=True, verbose_name=_('Description'))

    #: Основание
    reason = models.CharField(max_length=1024, null=True, blank=True, verbose_name=_('Reason'))

    # ###### Поля доставки #################

    #: Орагнизация-возчик
    carrier_code = models.CharField(max_length=64, null=True, blank=True, verbose_name=_('Carrier code'))
    carrier_name = models.CharField(max_length=255, null=True, blank=True, verbose_name=_('Carrier name'))

    #: Стоимость доставки
    delivery_cost = models.DecimalField(max_digits=15, decimal_places=2, default='0.00', null=True, blank=True,
                                        verbose_name=_('Delivery cost'))

    # ###### Поля дисконта #################

    #: Строка-идентификатор движка дисконтной системы
    dc_engine_id = models.CharField(max_length=128, null=True, blank=True, verbose_name=_('Discount system engine ID'))

    #: Номер бонусной карты участника
    dc_card_no = models.CharField(max_length=1024, null=True, blank=True, verbose_name=_('Discount card No'))

    #: Код авторизации
    auth_code = models.CharField(max_length=64, null=True, blank=True, verbose_name=_('Authorization code'))

    #: Промо-код
    promo_code = models.CharField(max_length=1024, null=True, blank=True, verbose_name=_('Promo code'))

    #: Итого начислено баллов
    total_accrued_bp = models.IntegerField(null=True, blank=True, default=0, verbose_name=_('Total accrued bonus points'))

    #: Документ является актуальной версией
    is_actual = models.BooleanField(default=True)

    objects = OrderManager()

    def is_succeeded_build(self):
        """
        Вернуть Истину если сборка по заказу была удачной. В противном случае вернуть Ложь
        """
        for row in self.rows.iterator():
            if not self.build_date:
                return None
            if row.difference:
                return False
        return True
    is_succeeded_build.short_description = _('Is succeeded build?')

    def recalculate_bp(self):
        """
        Рассчитать начисления в баллах
        """
        for engine_class in registry:
            if engine_class.id == self.dc_engine_id:
                engine = engine_class()
                if self.dc_card_no and self.auth_code:
                    engine.dc_card({'card_no': self.dc_card_no, 'auth_code': self.auth_code})
                engine.sp = self.ou_code
                engine.promo_code = self.promo_code
                last_result = 0
                for row in self.rows.iterator():
                    row.accrued_bp = engine.calculate_bp(
                        row.product_sku, row.product_name,
                        row.retail_price, row.quantity,
                        self.customer,
                        row.product_group_code, row.product_group_name,
                        row.product_category_code, row.product_category_name,
                        row.manufacturer_code,
                        last_result
                    )
                    row.save()
                    last_result = row.accrued_bp
                break

    def recalculate_discount(self):
        """
        Рассчитать скидки
        """
        for engine_class in registry:
            if engine_class.id == self.dc_engine_id:
                engine = engine_class()
                if self.dc_card_no and self.auth_code:
                    engine.dc_card({'card_no': self.dc_card_no, 'auth_code': self.auth_code})
                engine.sp = self.ou_code
                engine.promo_code = self.promo_code
                last_result = 0
                for row in self.rows.iterator():
                    row.discount_percentage = engine.calculate_discount(
                        row.product_sku, row.product_name,
                        row.retail_price, row.quantity,
                        self.customer,
                        row.product_group_code, row.product_group_name,
                        row.product_category_code, row.product_category_name,
                        row.manufacturer_code,
                        last_result
                    )
                    row.discount = row.retail_price*row.quantity/Decimal("100.00")*row.discount_percentage
                    row.total_cost = row.retail_price*row.quantity - row.discount
                    row.save()
                    last_result = row.discount_percentage
                break

    @transaction.atomic()
    def add_reserve(self):
        """
        Резерв товара
        """

        # Проводим заказ на регистре резерва
        try:
            location = StockLocation.objects.get(code=self.location_code)
        except ObjectDoesNotExist:
            pass
        else:
            product_index = {product.sku: product for product in Product.objects.filter(
                sku__in=self.rows.values_list("product_sku", flat=True))}
            for row in self.rows.iterator():
                try:
                    register = ReserveRegister.objects.get_or_create(
                        product__sku=row.product_sku, location=location,
                        defaults={
                            "actual_datetime": timezone.datetime.min.replace(tzinfo=timezone.get_current_timezone()),
                            "location": location,
                            "product": product_index[row.product_sku],
                        })[0]
                except IndexError:
                    raise ProductNotExists(row.product_sku)

                ReserveRegisterRow.objects.create(
                    effective_date=self.effective_date,
                    quantity=row.quantity,
                    doc_code=self.code,
                    short_desc=unicode(self),
                    register=register
                )

    @transaction.atomic()
    def remove_reserve(self):
        """
        Отмена проводки заказа
        """

        for row in ReserveRegisterRow.objects.filter(doc_code=self.code):
            row.delete()

    @transaction.atomic()
    def accept_payment_in_bp(self):
        """
        Принять платеж бонусными баллами
        """

        # Проводим заказ на бальном регистре
        for payment in self.payments.iterator():
            if payment.payment_type == 'BP':

                for engine_class in registry:
                    if engine_class.id == self.dc_engine_id:
                        engine = engine_class()
                        if self.dc_card_no and self.auth_code:
                            engine.dc_card({'card_no': self.dc_card_no, 'auth_code': self.auth_code})
                        engine.sp = self.ou_code
                        engine.promo_code = self.promo_code

                        payment.transaction_no = engine.account.rows.create(
                            effective_date=payment.effective_date,
                            bp=-payment.payment,
                            doc_object=self,
                        ).pk

                        payment.save()

                if not payment.transaction_no:
                    raise WrongOrderPayment("Wrong BP payment!")

    @transaction.atomic()
    def cancel_payment_in_bp(self):
        """
        Отмена платежа бонусными баллами
        """
        for payment in self.payments.filter(payment_type='BP'):
            for engine_class in registry:
                if engine_class.id == self.dc_engine_id:
                    engine = engine_class(self.dc_card_no, self.auth_code)
                    engine.account.rows.get(id=payment.transaction_no).delete()
                    payment.delete()

    @transaction.atomic()
    def accrue_bp(self):
        """
        Начислить баллов
        """
        # Проводим заказ на бальном регистре
        for payment in self.payments.iterator():
            if payment.payment_type == 'BP' and 'dc_engine' in registry:
                e = registry['dc_engine'](self.dc_card_no, self.auth_code)
                e.sp = self.ou_code
                e.promo_code = self.promo_code
                e.account.rows.append(
                            effective_date=payment.effective_date,
                            bp=payment.payment,
                            doc_object=self,
                )

    def get_workflow(self):
        # Вернуть, а если не существует, то создать, связанный с документом рабочий процесс

        # active_order = Order.objects.get_actual_instance(self.doc_no)

        workflow_schema = Schema.objects.get_or_create(
                code='goods_order',
                defaults={
                    "name": 'Заказ товара',
                    "code": 'goods_order',
                })

        workflow_instance = Workflow.objects.get_or_create(
                doc_id=self.id,
                defaults={
                    "doc_object": self,
                    "workflow_schema": workflow_schema[0] if type(workflow_schema) == tuple else workflow_schema,
                })
        return workflow_instance[0] if type(workflow_instance) == tuple else workflow_instance

    def is_original(self):
        """
        Возвращает Истина, если документ изготовлен локальным нодом,
        и возвращает Ложь если документ является репликой оригинала
        """
        return self.created_by == settings.ELASTIC_SETTINGS['server_name']

    def calculate_delivery_cost(self):
        """
        Вычисляем стоимость доставки
        """
        self.delivery_cost = Decimal("0.0")
        try:
            carrier = Carrier.objects.get(code=self.carrier_code)
        except ObjectDoesNotExist:
            pass
        else:
            for row in self.rows.all():
                if carrier and self.delivery_term:
                    self.delivery_cost += carrier.calculate_delivery_cost(
                        StockLocation.objects.get(code=self.location_code).party.addresses.first(),
                        self.delivery_term.delivery_location,
                        row.product_sku,
                        row.quantity
                    )

    def calculate_results(self):
        """
        Пересчитать итоги
        """
        self.total_cost = Decimal("0.0")
        for row in self.rows.all():
            self.total_cost += row.retail_price * row.quantity

        self.grand_total = self.total_cost + self.delivery_cost

    @transaction.atomic()
    def clone(self):
        """
        Клонировать заказ
        """

        FIELDS_TO_COPY = (
            'auth_code', 'carrier_name', 'carrier_code', 'consignee_name', 'consignee_phone',
            'currency', 'customer_login', 'customer_name', 'customer_phone', 'dc_card_no', 'dc_engine_id',
            'description', 'doc_no', 'location_code', 'location_name', 'ou_code', 'ou_name',
            'promo_code',
        )

        data = {}
        for field in FIELDS_TO_COPY:
            data[field] = getattr(self, field)

        now = timezone.now()

        self.is_actual = False
        self.save()

        order = Order.objects.create(
            reason=self.code,
            effective_date=now,
            expired_date=now + timezone.timedelta(days=3),
            is_actual=True,
            last_modify_date=now,
            **data
        )

        # Переносим платежи заказа
        for payment in self.payments.iterator():
            payment.id = None
            payment.order = order
            payment.save()

        # Переносим условия доставки
        try:
            dt = self.delivery_term
            dt.id = None
            dt.order = order
            dt.save()
        except DeliveryTerm.DoesNotExist:
            pass

        return order

    def __unicode__(self):
        return u"#{0} at {1}".format(self.doc_no, self.creation_date)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        # Устанавливаем внутренний код документа
        if not self.code:
            self.code = uuid.uuid1()

        # Устанавливаем дату создания
        if not self.creation_date:
            self.creation_date = timezone.now()

        # Пересчитываем дату истечения срока
        if not self.expired_date:
            self.expired_date = timezone.now() + timedelta(3)  # Счет действителен три дня с момента создания

        if not self.doc_no:
            # Устанавливаем номер документа
            s = Sequence.objects.get_or_create(
                name='order',
                defaults={
                    'name': 'order',
                    'prefix': 'Ord',
                    'separator': '-',
                }
            )[0]
            self.doc_no = s.request_next_number()

        # Устанавливаем дату последней модификации
        if not self.last_modify_date:
            self.last_modify_date = timezone.now()

        # Устанавливаем имя локального нода, в качестве нода-создателя документа
        if not self.created_by:
            self.created_by = settings.ELASTIC_SETTINGS['server_name']

        super(Order, self).save(force_insert, force_update, using, update_fields)


class OrderRow(models.Model):
    """
    Строка Заказа
    """
    class Meta:
        verbose_name = _('Order row')
        verbose_name_plural = _('Order rows')

    #: Складской код товара
    product_sku = models.CharField(max_length=1024, verbose_name=_('Product SKU'))

    #: Наименование товара
    product_name = models.CharField(max_length=1024, verbose_name=_('Product name'))

    #: Наименование товарной группы
    product_group_name = models.CharField(max_length=255, null=True, blank=True, verbose_name=_('Product group name'))

    #: Код товарной группы
    product_group_code = models.CharField(max_length=128, null=True, blank=True, verbose_name=_('Product group code'))

    #: Наименование товарной категории
    product_category_name = models.CharField(max_length=255, null=True, blank=True,
                                             verbose_name=_('Product category name'))

    #: Код товарной категории
    product_category_code = models.CharField(max_length=128, null=True, blank=True,
                                             verbose_name=_('Product category code'))

    #: Код производителя
    manufacturer_code = models.CharField(null=True, blank=True, max_length=1024, verbose_name=_('Manufacturer code'))

    #: Страна происхождения. Ссылка на элемент справочника "Страны мира"
    consignment_country = models.ForeignKey(Country, null=True, blank=True, verbose_name=_('Country of consignment'))

    #: Розничная цена
    retail_price = models.DecimalField(max_digits=15, decimal_places=2, verbose_name=_('Retail price'))

    #: Единица измерения. Ссылка на элемент справочника "Единицы измерения"
    uom = models.ForeignKey(Uom, verbose_name=_("Unit of measures"))

    #: Количество
    quantity = models.DecimalField(max_digits=12, decimal_places=3, verbose_name=_('Quantity'))

    #: Количество товара, который сборщик не смог найти
    difference = models.DecimalField(max_digits=12, decimal_places=3, null=True, blank=True,
                                     verbose_name=_('Difference'))

    #: Стоимость (с учетом скидки)
    total_cost = models.DecimalField(max_digits=15, decimal_places=2, verbose_name=_('Total cost'))

    #: Заказ
    order = models.ForeignKey(Order, related_name='rows', verbose_name=_('Order'))

    # ###### Поля дисконта #################
    #: Скидка в процентах
    discount_percentage = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True,
                                              verbose_name=_('Discount percentage'))

    #: Скидка в рублях
    discount = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True, verbose_name=_('Discount'))

    #: Начислено баллов
    accrued_bp = models.IntegerField(null=True, blank=True, default=0, verbose_name=_('Accrued bonus points'))

    def __unicode__(self):
        return u"{0}".format(self.product_name)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # Для новой строчки указываем стоимость без скидки; в дальнейшем дисконт может модифицировать ее
        if not self.id:
            self.total_cost = self.quantity * self.retail_price

        super(OrderRow, self).save(force_insert, force_update, using, update_fields)

        # Если документ был сделан на текущем ноде,
        # то расчитываем стоимость доставки и пересчитываем итоги
        if self.order.is_original():
            self.order.calculate_delivery_cost()
            self.order.calculate_results()
            self.order.save()


class Payment(models.Model):
    """
    Платеж заказа
    """
    class Meta:
        verbose_name = _('Payment')
        verbose_name_plural = _('Payments')

    PAYMENT_TYPE = (
        ('bp', _('Bonus point')),  # Оплата бонусными баллами
        ('cert', _('Certified')),  # Оплата сертификатом
        ('eft', _('Electronic funds transfer')),  # Электронный перевод денежных средств
        ('cash', _('Cash')),  # Наличный расчет
    )

    #: Дата/время создания
    creation_date = models.DateTimeField(verbose_name=_('Creation date'))

    #: Стоимость (с учетом скидки)
    payment = models.DecimalField(max_digits=15, decimal_places=2, verbose_name=_('Payment'))

    #: Тип платежа
    payment_type = models.CharField(max_length=8, choices=PAYMENT_TYPE, verbose_name=_('Payment type'))

    #: Номер транзакции
    transaction_no = models.CharField(max_length=128, null=True, blank=True, verbose_name=_('Transaction No'))

    #: Заказ
    order = models.ForeignKey(Order, related_name='payments', verbose_name=_('Order'))


class DeliveryTerm(models.Model):
    """
    Условие доставки
    """
    class Meta:
        verbose_name = _('Delivery term')
        verbose_name_plural = _('Delivery terms')

    TERMS = (
        ('EXW', _('Ex worker')),  # франко-склад, франко-завод: товар забирается покупателем с указанного в договоре
                                  #  склада продавца, оплата экспортных пошлин вменяется в обязанность покупателю
        ('FCA', _('Free carrier')),  # франко-перевозчик: товар доставляется основному перевозчику заказчика,
                                     # к указанному в договоре терминалу отправления, экспортные пошлины уплачивает
                                     # продавец.
        ('CPT', _('Carriage paid to')),  # товар доставляется основному перевозчику заказчика, основную перевозку
                                         # до указанного в договоре терминала прибытия оплачивает продавец, расходы по
                                         # страховке несёт покупатель, импортную растаможку и доставку с терминала
                                         # прибытия основного перевозчика осуществляет покупатель
        ('CIP', _('Carriage and insurance paid to')),  # то же, что CPT, но основная перевозка страхуется продавцом
        ('DAT', _('Delivered at terminal')),  # поставка до указанного в договоре импортного таможенного терминала
                                              # оплачена, то есть экспортные платежи и основную перевозку, включая
                                              # страховку оплачивает продавец, таможенная очистка по импорту
                                              # осуществляется покупателем
        ('DAP', _('Delivered at point')),  # поставка в место назначения, указанном в договоре, импортные пошлины и
                                           #  местные налоги оплачиваются покупателем
        ('DDP', _('Delivered duty paid')),  # товар доставляется заказчику в место назначения, указанном в договоре,
                                            # очищенный от всех пошлин и рисков
        ('FOB', _('Free on board')),  # товар отгружается на судно покупателя, перевалку оплачивает продавец
        ('FAS', _('Free alongside ship')),  # товар доставляется к судну покупателя, в договоре указывается порт
                                            # погрузки, перевалку и погрузку оплачивает покупатель
        ('CFR', _('Cost and freight')),  # товар доставляется до указанного в договоре порта назначения покупателя,
                                         # страховку основной перевозки, разгрузку и перевалку оплачивает покупатель
        ('CIF', _('Cost, Insurance and Freight')),  # то же, что CFR, но основную перевозку страхует продавец
    )

    incoterms_code = models.CharField(verbose_name=_('INCOTERMS code'), max_length=3, choices=TERMS, default='EXW')
    alternative_delivery_terms_code = models.CharField(verbose_name=_('Alternative delivery terms code'),
                                                       null=True, blank=True, max_length=56)
    delivery_location = models.ForeignKey(Address, null=True, blank=True, verbose_name=_('Delivery location'))
    delivery_instruction = models.TextField(null=True, blank=True, verbose_name=_('Delivery instruction'))
    order = models.OneToOneField(Order, related_name='delivery_term', verbose_name=_('Order'))

    def __unicode__(self):
        return u"{0} {1}".format(self.incoterms_code, self.delivery_location.city)


class Sequence(models.Model):
    class Meta:
        verbose_name = _('Sequence')
        verbose_name_plural = _('Sequences')

    name = models.CharField(max_length=255, verbose_name=_('Name'))
    separator = models.CharField(default='-', blank=True, max_length=1, verbose_name=_('Separator'))
    prefix = models.CharField(max_length=8, null=True, blank=True, verbose_name=_('Prefix'))
    number = models.IntegerField(editable=False, default=0, verbose_name=_('Number'))
    suffix = models.CharField(max_length=8, null=True, blank=True, verbose_name=_('Suffix'))

    @transaction.atomic
    def request_next_number(self):
        s = Sequence.objects.select_for_update().get(id=self.id)
        return s.get_next_number()

    def current_number(self):
        if self.prefix and self.suffix:
            return u"{0}{3}{1}{3}{2}".format(self.prefix, self.number, self.suffix, self.separator)
        if self.prefix:
            return u"{0}{2}{1}".format(self.prefix, self.number, self.separator)
        if self.suffix:
            return u"{0}{2}{1}".format(self.number, self.suffix, self.separator)
        return u"{0}".format(self.number)
    current_number.short_description = _('Current number')

    def get_next_number(self, force_insert=False, force_update=False, using=None):
        self.number += 1
        super(Sequence, self).save(force_insert, force_update, using)
        return self.current_number()

    def reset_sequence(self, force_insert=False, force_update=False, using=None):
        self.number = 0
        super(Sequence, self).save(force_insert, force_update, using)

    def __unicode__(self):
        return self.name
