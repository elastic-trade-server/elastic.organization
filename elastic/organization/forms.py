# -*- coding: utf-8 -*-

from autocomplete_light import forms
from models import *
import django

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Organization package"


class OuForm(forms.ModelForm):
    class Meta:
        model = OU
        if django.VERSION >= (1, 6):
            fields = '__all__'


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        if django.VERSION >= (1, 6):
            fields = '__all__'


class OrderRowForm(forms.ModelForm):
    class Meta:
        model = Order
        if django.VERSION >= (1, 6):
            fields = '__all__'


class DeliveryTermForm(forms.ModelForm):
    class Meta:
        model = Order
        if django.VERSION >= (1, 6):
            fields = '__all__'
