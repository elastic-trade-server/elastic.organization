from django.conf.urls import patterns, include, url
from tastypie.api import Api
from api import *

v1_api = Api(api_name='v1')
v1_api.register(BankAccountResource())
v1_api.register(OrganizationResource())
v1_api.register(CounterpartyResource())
v1_api.register(CounterpartyAdditionalParamResource())
v1_api.register(OuResource())
v1_api.register(EmployeeResource())
v1_api.register(OrderPaymentResource())
v1_api.register(OrderRowResource())
v1_api.register(OrderResource())
v1_api.register(DeliveryTermResource())


urlpatterns = patterns(
    '',
    url(r'^api/', include(v1_api.urls)),
)
