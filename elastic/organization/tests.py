from tastypie.test import ResourceTestCase
from django.contrib.auth.models import User
from django.db import transaction
from copy import copy
import json


class OrganizationApiTestCase(ResourceTestCase):
    fixtures = ['currency/initial_data.json','organization/test_data.json']

    user = None
    username = 'test-user'
    password = 'test-password'
    resource_url = '/api/v1/order/'

    post_data = {
        'doc_no': 'test-doc-no',
        'code': 'test-code',
        'creation_date': '2016-01-01T00:00:00',
        'last_modify_date': '2016-01-02T00:00:00',
        'expired_date': '2016-01-03T00:00:00',
        'location_code': 'test-location-code',
        'location_name': 'test-location-name',
        'ou_code': 'test-ou-code',
        'ou_name': 'test-ou-name',
        'customer_name': 'test-customer-name',
        'customer_login': 'test-customer-login',
        'customer_phone': 'test-customer-phone',
        'total_cost': '0.00',
        'grand_total': '0.00'
    }

    default_data = {
        'delivery_cost': '0.00',
        'total_accrued_bp': 0
    }

    @classmethod
    def setUpClass(cls):
        super(OrganizationApiTestCase, cls).setUpClass()
        cls.user = User.objects.create_user(username=cls.username, password=cls.password)

    @classmethod
    def tearDownClass(cls):
        super(OrganizationApiTestCase, cls).tearDownClass()
        cls.user.delete()

    def get_credentials(self):
        return self.create_basic(username=self.username, password=self.password)

    def test_create_order_with_minimum_fields(self):
        resp = self.api_client.post(self.resource_url, format='json', data=self.post_data,
                                    authentication=self.get_credentials(), follow=True)
        self.assertHttpCreated(resp)
        resp = self.api_client.get(resp.get('location'), format='json', authentication=self.get_credentials())
        data = self.deserialize(resp)
        for field, value in self.post_data.items() + self.default_data.items():
            self.assertIn(field, data)
            self.assertEqual(data[field], value, 'Field %s in %s' % (field, json.dumps(data)))

    def test_error_with_miss_required_fields(self):
        for field in self.post_data.iterkeys():
            post_data = copy(self.post_data)
            del post_data[field]
            resp = self.api_client.post(self.resource_url, format='json', data=post_data,
                                        authentication=self.get_credentials())
            self.assertEqual(resp.status_code, 201, 'Created with missing required field "%s"' % field)

    def test_error_with_null_required_fields(self):
        for field in self.post_data.iterkeys():
            if field == 'code':
                continue

            with transaction.atomic():
                post_data = copy(self.post_data)
                post_data[field] = None
                resp = self.api_client.post(self.resource_url, format='json', data=post_data,
                                            authentication=self.get_credentials())
                self.assertEqual(resp.status_code, 400, 'Created with null value field "%s"' % field)
                transaction.rollback()
