# -*- coding: utf-8 -*-

from models import *
from django.utils import timezone
from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from tastypie.authentication import SessionAuthentication, BasicAuthentication, MultiAuthentication
from tastypie.authorization import Authorization
from django.utils.translation import ugettext_lazy as _
from elastic.currency.api import CurrencyResource
from django.conf.urls import url
from tastypie.utils.urls import trailing_slash
from tastypie.bundle import Bundle
from tastypie.exceptions import NotFound
from dateutil.parser import parse
from tastypie.utils import make_aware
import signals


__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Organization package"


class BankAccountResource(ModelResource):
    class Meta:
        queryset = BankAccount.objects.all()
        resource_name = 'bank_account'
        authentication = MultiAuthentication(BasicAuthentication(), SessionAuthentication())
        authorization = Authorization()
        excludes = ['id', ]
        include_resource_uri = False
        filtering = {
            "account": ALL,
            "c_account": ALL,
            "bank_code": ALL,
            "organization": ALL_WITH_RELATIONS,
        }

    account = fields.CharField(attribute='account', help_text=_('Code'))
    c_account = fields.CharField(attribute='c_account', help_text=_('Name'))
    bank_name = fields.CharField(attribute='bank_name', help_text=_('Bank name'))
    bank_code = fields.CharField(attribute='bank_code', help_text=_('Bank code'))
    organization = fields.ToOneField('elastic.organization.api.OrganizationResource', attribute='organization')

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<account>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.account
        else:
            kwargs['pk'] = bundle_or_obj.account

        return kwargs

    def dehydrate(self, bundle):
        bundle.data = dict(filter(lambda item: item[1] is not None, bundle.data.iteritems()))
        bundle.data.pop('organization')
        return bundle


class OrganizationResource(ModelResource):
    class Meta:
        queryset = Organization.objects.all()
        list_allowed_methods = ['get', ]
        resource_name = 'organization'
        authentication = MultiAuthentication(BasicAuthentication(), SessionAuthentication())
        authorization = Authorization()
        excludes = ['id', ]
        include_resource_uri = False
        filtering = {
            "reg_no": ALL,
            "bank_accounts": ALL_WITH_RELATIONS
        }

    name = fields.CharField(attribute='name', help_text=_('Name'))
    reg_no = fields.CharField(attribute='reg_no', unique=True, help_text=_('Registration number'))
    bank_accounts = fields.ToManyField(to=BankAccountResource, attribute='bank_accounts', related_name='organization',
                                       full=True, null=True, blank=True, help_text=_('Organization'))

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<reg_no>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.reg_no
        else:
            kwargs['pk'] = bundle_or_obj.reg_no

        return kwargs

    def dehydrate(self, bundle):
        bundle.data = dict(filter(lambda item: item[1] is not None, bundle.data.iteritems()))
        return bundle

    @staticmethod
    def hydrate_bank_accounts(bundle):
        bundle.obj.bank_accounts.all().delete()
        return bundle


class CounterpartyResource(ModelResource):
    class Meta:
        queryset = Counterparty.objects.all()
        list_allowed_methods = ['get', 'post', ]
        resource_name = 'counterparty'
        authentication = MultiAuthentication(BasicAuthentication(), SessionAuthentication())
        authorization = Authorization()
        excludes = ['id', ]
        include_resource_uri = False
        filtering = {
            "id": ALL,
            "party": ALL_WITH_RELATIONS,
            "organization": ALL_WITH_RELATIONS,
        }

    username = fields.CharField(help_text=_('Username'))
    password = fields.CharField(help_text=_('Password hash'))
    party = fields.ToOneField(to='elastic.party.api.PartyResource', attribute='party', null=True, blank=True,
                              full=True, help_text=_('Party'))
    organization = fields.ToOneField(OrganizationResource, attribute='organization',
                                     null=True, blank=True, full=True, help_text=_('Organization'))
    additional_params = fields.ToManyField(to='elastic.organization.api.CounterpartyAdditionalParamResource',
                                           attribute='additional_params', related_name='counterparty', full=True,
                                           null=True, blank=True, help_text=_('Additional params'))
    sync = fields.BooleanField(null=True,  blank=True, default=True)

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<username>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.user.username
        else:
            kwargs['pk'] = bundle_or_obj.user.username

        return kwargs

    def obj_get(self, bundle, **kwargs):
        kwargs['pk'] = Counterparty.objects.get(**{"user__username": kwargs['username']}).pk
        return super(CounterpartyResource, self).obj_get(bundle, **kwargs)

    def dehydrate_username(self, bundle):
        return bundle.obj.user.username

    def dehydrate_password(self, bundle):
        return bundle.obj.user.password

    @staticmethod
    def hydrate_party(bundle):
        if bundle.data.get('party', None):
            try:
                # Добавляем значение первичного ключа для последующего обновления записи с совпадающим именем
                bundle.data['party']['pk'] = Party.objects.get(name=bundle.data['party'].get('name', None)).pk
            except ObjectDoesNotExist:
                pass
        return bundle

    @staticmethod
    def hydrate_organization(bundle):
        if bundle.data.get('organization', None):
            try:
                # Добавляем значение первичного ключа для последующего обновления записи с совпадающим ОГРН/ОГРНИП
                bundle.data['organization']['pk'] = Organization.objects.get(
                    reg_no=bundle.data['organization'].get('reg_no', None)).pk
            except ObjectDoesNotExist:
                pass
        return bundle

    def hydrate(self, bundle):
        # Обрабатываем поле "Пользователь сайта"
        try:
            bundle.obj.user = User.objects.get(username=bundle.data.get('username', None))
            bundle.data.pop('user') if 'user' in bundle.data else None
        except ObjectDoesNotExist:
            bundle.obj.user = User.objects.create_user(username=bundle.data.get('username', None))

        bundle.obj.user.password = bundle.data.get('password')
        bundle.obj.user.save()
        bundle.data.pop('password')

        return bundle

    @transaction.atomic
    def obj_create(self, bundle, **kwargs):
        try:
            bundle.obj = Counterparty.objects.get(user__username=bundle.data.get('username', None))
        except ObjectDoesNotExist:
            bundle = super(CounterpartyResource, self).obj_create(bundle, **kwargs)

            if bundle.data.get('sync', True):
                # Запрашиваем репликацию
                signals.replication_request.send(sender=bundle.obj)

            return bundle

        return self.obj_update(bundle, **kwargs)

    @transaction.atomic
    def obj_update(self, bundle, **kwargs):
        try:
            bundle.obj = Counterparty.objects.get(user__username=bundle.data.get('username', None))
            kwargs['pk'] = bundle.obj.pk
        except ObjectDoesNotExist:
            raise NotFound('Counterpatry with username "{0}" not found!'.format(bundle.data.get('username', None)))

        # Если дата последней модификации сохранненого объекта страше даты последней модификации обновления...
        last_modify_date = make_aware(parse(bundle.data['last_modify_date']))
        if bundle.obj.last_modify_date >= last_modify_date:
            # ....то молча игнорируем обновление
            return bundle

        # Убиваем все дополнительные параметры, - они должны быть переписаны
        for p in bundle.obj.additional_params.iterator():
            p.delete()

        bundle = super(CounterpartyResource, self).obj_update(bundle, **kwargs)

        if bundle.data.get('sync', True):
            # Запрашиваем репликацию
            signals.replication_request.send(sender=bundle.obj)

        return bundle

    def obj_delete(self, bundle, **kwargs):
        try:
            bundle.obj = Counterparty.objects.get(**kwargs)
        except ObjectDoesNotExist:
            return

        if bundle.data.get('sync', True):
            # Запрашиваем репликацию
            signals.replication_request.send(sender=bundle.obj, **{'is_removing': True})
        bundle.obj.delete()

    def dehydrate(self, bundle):
        bundle.data = dict(filter(lambda item: item[1] is not None, bundle.data.iteritems()))
        return bundle


class CounterpartyAdditionalParamResource(ModelResource):
    class Meta:
        queryset = CounterpartyAdditionalParam.objects.all()
        resource_name = 'counterparty_additional_param'
        authentication = MultiAuthentication(BasicAuthentication(), SessionAuthentication())
        authorization = Authorization()
        excludes = ['id', ]
        include_resource_uri = False
        filtering = {
            "name": ALL,
            "value": ALL,
        }

    name = fields.CharField(attribute='name', help_text=_('Name'))
    value = fields.CharField(attribute='value', help_text=_('Value'))
    counterparty = fields.ToOneField('elastic.organization.api.CounterpartyResource', attribute='counterparty')

    def dehydrate(self, bundle):
        bundle.data.pop('counterparty')
        return bundle


class OuResource(ModelResource):
    class Meta:
        queryset = OU.objects.all()
        list_allowed_methods = ['get', ]
        resource_name = 'ou'
        authentication = SessionAuthentication()
        authorization = Authorization()
        excludes = ['lft', 'level', 'tree_id', 'rght', 'id', ]
        filtering = {
            "code": ALL,
            "organization": ALL_WITH_RELATIONS,
            "party": ALL_WITH_RELATIONS,
        }

    parent = fields.ToOneField('self', 'parent', null=True, blank=True, full=True, help_text=_('Parent'))
    name = fields.CharField(attribute='name', help_text=_('Name'))
    code = fields.CharField(attribute='code', help_text=_('OU code'))
    organization = fields.ToOneField(to="elastic.organization.api.OrganizationResource", attribute='organization',
                                     null=True, blank=True, full=True, help_text=_('Organization'))
    party = fields.ToOneField(to='elastic.party.api.PartyResource', attribute='party', null=True, blank=True,
                              full=True, help_text=_('Party'))

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<code>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.code
        else:
            kwargs['pk'] = bundle_or_obj.code

        return kwargs


class EmployeeResource(ModelResource):
    class Meta:
        queryset = Employee.objects.all()
        resource_name = 'employee'
        list_allowed_methods = ['get', ]
        authentication = SessionAuthentication()
        authorization = Authorization()
        excludes = ['id', ]
        include_resource_uri = False
        filtering = {
            "code": ALL,
            "party": ALL_WITH_RELATIONS,
            "user": ALL_WITH_RELATIONS,
            "rank": ALL_WITH_RELATIONS,
            "ou": ALL_WITH_RELATIONS,
        }

    code = fields.CharField(attribute='code', help_text=_('Personnel number'))
    party = fields.ToOneField(to='elastic.party.api.PartyResource', attribute='party', null=True, blank=True,
                              full=True, help_text=_('Party'))
    rank = fields.CharField(attribute='rank', help_text=_('Rank'))
    ou = fields.ToOneField(OuResource, 'ou', full=True, help_text=_('Organization unit'))

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<code>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.code
        else:
            kwargs['pk'] = bundle_or_obj.code

        return kwargs


class OrderPaymentResource(ModelResource):
    class Meta:
        queryset = Payment.objects.all()
        resource_name = 'order_payment'
        authentication = MultiAuthentication(BasicAuthentication(), SessionAuthentication())
        authorization = Authorization()
        excludes = ['id', ]
        include_resource_uri = False
        filtering = {
            "payment_type": ALL,
            "transaction_no": ALL,
            "order": ALL_WITH_RELATIONS,
        }

    creation_date = fields.DateTimeField(attribute='creation_date', help_text=_('Creation date'))
    payment = fields.DecimalField(attribute='payment', help_text=_('Payment amount'))
    payment_type = fields.CharField(attribute='payment_type', help_text=_('Payment type'))
    transaction_no = fields.CharField(attribute='transaction_no', help_text=_('Transaction No'))
    order = fields.ToOneField("elastic.organization.api.OrderResource", 'order', help_text=_('Order'))

    def dehydrate(self, bundle):
        bundle.data.pop('order')
        return bundle

    def alter_detail_data_to_serialize(self, request, bundle):
        bundle.data = dict(filter(lambda item: item[1] is not None, bundle.data.iteritems()))
        return bundle


class OrderRowResource(ModelResource):
    class Meta:
        queryset = OrderRow.objects.all()
        resource_name = 'order_row'
        authentication = MultiAuthentication(BasicAuthentication(), SessionAuthentication())
        authorization = Authorization()
        excludes = ['id', ]
        include_resource_uri = False
        filtering = {
            "product_sku": ALL,
            "product_group_code": ALL,
            "product_category_code": ALL,
            "manufacturer_code": ALL,
            "order": ALL_WITH_RELATIONS,
        }

    product_sku = fields.CharField(attribute='product_sku', help_text=_('Product SKU'))
    product_name = fields.CharField(attribute='product_name', help_text=_('Product name'))

    product_group_name = fields.CharField(attribute='product_group_name', null=True, blank=True,
                                          help_text=_('Product group name'))
    product_group_code = fields.CharField(attribute='product_group_code', null=True, blank=True,
                                          help_text=_('Product group code'))
    product_category_name = fields.CharField(attribute='product_category_name', null=True, blank=True,
                                             help_text=_('Product category name'))
    product_category_code = fields.CharField(attribute='product_category_code', null=True, blank=True,
                                             help_text=_('Product category code'))
    manufacturer_code = fields.CharField(attribute='manufacturer_code', help_text=_('Manufacturer code'))

    consignment_country = fields.ToOneField("elastic.country.api.CountryResource", attribute='consignment_country',
                                            null=True, blank=True, full=True, help_text=_('Country of consignment'))
    retail_price = fields.DecimalField(attribute='retail_price', help_text=_('Retail price'))
    uom = fields.ToOneField("elastic.product.api.UomResource", attribute='uom', full=True,
                            help_text=_('Unit of Measures'))
    quantity = fields.DecimalField(attribute='quantity', help_text=_('Quantity'))
    difference = fields.DecimalField(attribute='difference', null=True, blank=True, help_text=_('Quantity'))
    total_cost = fields.DecimalField(attribute='total_cost', help_text=_('Total cost'))

    # ###### Поля дисконта #################
    discount_percentage = fields.DecimalField(attribute='discount_percentage', null=True, blank=True,
                                              help_text=_('Discount percentage'))
    discount = fields.DecimalField(attribute='discount', null=True, blank=True, help_text=_('Discount'))
    accrued_bp = fields.IntegerField(attribute='accrued_bp', null=True, blank=True, default=0,
                                     help_text=_('Accrued BP'))

    order = fields.ToOneField("elastic.organization.api.OrderResource", 'order', help_text=_('Order'))

    @staticmethod
    def dehydrate_uom(bundle):
        return bundle.obj.uom.code if bundle.obj.uom else None

    @staticmethod
    def hydrate_uom(bundle):
        if bundle.data.get('uom', None):
            bundle.obj.uom = Uom.objects.get(code=bundle.data['uom'])
            bundle.data.pop('uom')
        else:
            bundle.obj.uom = Uom.objects.get(code=796)
        return bundle

    @staticmethod
    def dehydrate_consignment_country(bundle):
        return bundle.obj.consignment_country.code if bundle.obj.consignment_country else None

    @staticmethod
    def hydrate_consignment_country(bundle):
        if bundle.data.get('consignment_country', None):
            bundle.obj.consignment_country = Country.objects.get(code=bundle.data['consignment_country'])
            bundle.data.pop('consignment_country')
        else:
            bundle.obj.consignment_country = Country.objects.get(code=1)
        return bundle

    def dehydrate(self, bundle):
        bundle.data.pop('order')
        return bundle

    def alter_detail_data_to_serialize(self, request, bundle):
        bundle.data = dict(filter(lambda item: item[1] is not None, bundle.data.iteritems()))
        return bundle


class OrderResource(ModelResource):
    class Meta:
        queryset = Order.objects.all()
        resource_name = 'order'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        allowed_methods = ['get', 'delete', 'post']
        excludes = ['id', ]
        include_resource_uri = False
        filtering = {
            "doc_no": ALL,
            "code": ALL,
            "create_by": ALL,
            "customer": ALL,
            "rows": ALL_WITH_RELATIONS,
            "payments": ALL_WITH_RELATIONS,
            "delivery_term": ALL_WITH_RELATIONS,
        }

    doc_no = fields.CharField(attribute='doc_no', help_text=_('Document number'))
    code = fields.CharField(attribute='code', help_text=_('Document internal code'))
    creation_date = fields.DateTimeField(attribute='creation_date', help_text=_('Creation date'))
    last_modify_date = fields.DateTimeField(attribute='last_modify_date', help_text=_('Last modify date'))
    expired_date = fields.DateTimeField(attribute='expired_date', help_text=_('Expired date'))
    effective_date = fields.DateTimeField(attribute='effective_date', null=True, blank=True,
                                          help_text=_('Effective date'))
    due_date = fields.DateTimeField(attribute='due_date', null=True, blank=True, help_text=_('Due date'))
    created_by = fields.CharField(attribute='created_by', null=True, blank=True,
                                  help_text=_('Created by'))
    location_code = fields.CharField(attribute='location_code', help_text=_('Location code'))
    location_name = fields.CharField(attribute='location_name', help_text=_('Location name'))
    ou_code = fields.CharField(attribute='ou_code', help_text=_('OU code'))
    ou_name = fields.CharField(attribute='ou_name', help_text=_('OU name'))
    customer_name = fields.CharField(attribute='customer_name', help_text=_('Customer name'))
    customer_login = fields.CharField(attribute='customer_login', null=True, blank=True, help_text=_('Customer login'))
    customer_phone = fields.CharField(attribute='customer_phone', null=True, blank=True, help_text=_('Customer phone'))
    consignee_name = fields.CharField(attribute='consignee_name', null=True, blank=True,
                                      help_text=_('Consignee name'))
    consignee_phone = fields.CharField(attribute='consignee_phone', null=True, blank=True,
                                       help_text=_('Consignee phone'))
    currency = fields.ToOneField(CurrencyResource, attribute='currency', null=True, blank=True,
                                 full=True, help_text=_('Currency'))
    total_cost = fields.DecimalField(attribute='total_cost', help_text=_('Total cost'))
    grand_total = fields.DecimalField(attribute='grand_total', help_text=_('Grand total'))
    build_date = fields.DateTimeField(attribute='build_date', null=True, blank=True, help_text=_('Build date'))
    description = fields.CharField(attribute='description', null=True, blank=True, help_text=_('Description'))
    carrier_name = fields.CharField(attribute='carrier_name', null=True, blank=True, help_text=_('Carrier name'))
    carrier_code = fields.CharField(attribute='carrier_code', null=True, blank=True, help_text=_('Carrier code'))
    delivery_term = fields.ToOneField(to='elastic.organization.api.DeliveryTermResource',
                                      attribute='delivery_term', null=True, blank=True, full=True,
                                      related_name='order', help_text=_('Delivery term'))
    delivery_cost = fields.DecimalField(attribute='delivery_cost', blank=False, help_text=_('Delivery cost'))
    dc_card_no = fields.CharField(attribute='dc_card_no', null=True, blank=True, help_text=_('Discount card No'))
    auth_code = fields.CharField(attribute='auth_code', null=True, blank=True, help_text=_('Authorization code'))
    promo_code = fields.CharField(attribute='promo_code', null=True, blank=True, help_text=_('Promo code'))
    total_accrued_bp = fields.IntegerField(attribute='total_accrued_bp', default=0, blank=True,
                                           help_text=_('Total accrued BP'))

    rows = fields.ToManyField(OrderRowResource, attribute='rows', related_name='order', full=True,
                              null=True, blank=True, help_text=_('Order rows'))

    payments = fields.ToManyField(OrderPaymentResource, attribute='payments', related_name='order', full=True,
                                  null=True, blank=True, help_text=_('Order payments'))
    state_name = fields.CharField(help_text=_('Workflow state name'), readonly=True)
    state_code = fields.CharField(help_text=_('Workflow state code'), readonly=True)
    sync = fields.BooleanField(null=True,  blank=True, default=True)

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<code>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.code
        else:
            kwargs['pk'] = bundle_or_obj.code

        return kwargs

    @staticmethod
    def dehydrate_state_name(bundle):
        w = bundle.obj.get_workflow()
        w.start()
        return w.get_current_state_name()

    @staticmethod
    def dehydrate_state_code(bundle):
        w = bundle.obj.get_workflow()
        w.start()
        return w.get_current_state_code()

    @staticmethod
    def dehydrate_currency(bundle):
        return bundle.obj.currency.code

    @staticmethod
    def hydrate_currency(bundle):
        if bundle.data.get('currency', None):
            bundle.obj.currency = Currency.objects.get(code=bundle.data['currency'])
            bundle.data.pop('currency')
        else:
            bundle.obj.currency = Currency.objects.get(code=643)
        return bundle

    @staticmethod
    def hydrate_created_by(bundle):
        if bundle.data.get('sync', True):
            # Устанавливаем имя локального нода, в качестве нода-создателя документа
            bundle.obj.created_by = settings.ELASTIC_SETTINGS['server_name']
        else:
            bundle.obj.created_by = bundle.data.get('created_by', None)
        return bundle

    @transaction.atomic
    def obj_create(self, bundle, **kwargs):
        try:
            bundle.obj = Order.objects.get(code=bundle.data.get('code', None))
        except ObjectDoesNotExist:

            bundle = super(OrderResource, self).obj_create(bundle, **kwargs)

            # Продвигаем документ
            signals.workflow_restart.send(instance=bundle.obj, sender=OrderResource)

            if bundle.data.get('sync', True):
                # Запрашиваем репликацию
                signals.replication_request.send(sender=bundle.obj)

            return bundle

        return self.obj_update(bundle, **{'pk': bundle.obj.pk, 'code': bundle.obj.code})

    @transaction.atomic
    def obj_update(self, bundle, **kwargs):
        try:
            bundle.obj = Order.objects.get(code=bundle.data['code'])
        except ObjectDoesNotExist:
            raise NotFound("Order with code #{0} not exists!".format(bundle.data['code']))

        # Если дата последней модификации сохранненого объекта страше даты последней модификации обновления...
        last_modify_date = make_aware(parse(bundle.data.get('last_modify_date', timezone.datetime.min.isoformat())))
        if bundle.obj.last_modify_date >= last_modify_date:
            # ....то молча игнорируем обновление
            return bundle

        # Удаляем старые условия доставки
        bundle.obj.delivery_term.delete()

        bundle = super(OrderResource, self).obj_update(bundle, **kwargs)

        # Продвигаем документ
        signals.workflow_restart.send(instance=bundle.obj, sender=OrderResource)

        if bundle.data.get('sync', True):
            # Запрашиваем репликацию
            signals.replication_request.send(sender=bundle.obj)

        return bundle

    @transaction.atomic
    def obj_delete(self, bundle, **kwargs):
        try:
            bundle.obj = Order.objects.get(**kwargs)
        except ObjectDoesNotExist:
            return

        if bundle.data.get('sync', True):
            # Запрашиваем репликацию
            signals.replication_request.send(sender=bundle.obj, **{'is_removing': True})
        bundle.obj.delete()

    def dehydrate(self, bundle):
        bundle.data = dict(filter(lambda item: item[1] is not None, bundle.data.iteritems()))
        return bundle


class DeliveryTermResource(ModelResource):
    class Meta:
        queryset = DeliveryTerm.objects.all()
        resource_name = 'delivery_term'
        authentication = SessionAuthentication()
        authorization = Authorization()
        excludes = ['id', ]
        include_resource_uri = False
        filtering = {
            "incoterms_code": ALL,
            "delivery_location": ALL_WITH_RELATIONS,
            "order": ALL_WITH_RELATIONS,
        }

    incoterms_code = fields.CharField(attribute='incoterms_code', help_text=_('INCOTERMS code'))
    alternative_delivery_terms_code = fields.CharField(attribute='alternative_delivery_terms_code',
                                                       help_text=_('Alternative delivery terms code code'))
    delivery_location = fields.ToOneField("elastic.address.api.AddressResource", attribute='delivery_location',
                                          null=True, blank=True, full=True, help_text=_('Addresses collection'))
    delivery_instruction = fields.CharField(attribute='delivery_instruction', null=True, blank=True,
                                            help_text=_('Delivery instruction'))
    order = fields.ToOneField("elastic.organization.api.OrderResource", "order", related_name="delivery_term",
                              null=True, blank=True, full=False, help_text=_('Order'))

    def dehydrate(self, bundle):
        bundle.data.pop('order')
        return bundle

    def alter_detail_data_to_serialize(self, request, bundle):
        bundle.data = dict(filter(lambda item: item[1] is not None, bundle.data.iteritems()))
        return bundle
