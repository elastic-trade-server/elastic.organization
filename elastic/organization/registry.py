# -*- coding: utf-8 -*-

from django.utils.module_loading import autodiscover_modules

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Organization package"


class DcSingletonMeta(type):
    def __init__(cls, name, bases, d):
        super(DcSingletonMeta, cls).__init__(name, bases, d)
        cls.instance = None

    def __call__(cls, *args):
        if cls.instance is None: # FIXME race condition
            cls.instance = super(DcSingletonMeta, cls).__call__(*args)
        return cls.instance


class DcRegistrySingleton(list):
    __metaclass__ = DcSingletonMeta

    def register(self, dc_engine):
        try:
            self.index(dc_engine)
        except ValueError:
            self.append(dc_engine)


registry = DcRegistrySingleton()


def register(dc_engine):
    DcRegistrySingleton().register(dc_engine)


def autodiscover():
    autodiscover_modules('elastic_dc_engine_registry')
